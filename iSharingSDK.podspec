Pod::Spec.new do |s|
  s.name             = 'iSharingSDK'
  s.version          = '0.1.0'
  s.summary          = 'none'
 
  s.description      = <<-DESC
  This is a description.
                       DESC
 
  s.homepage         = 'https://google.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '<NAME HERE>' => '<EMAIL HERE>' }
  s.source           = { :git => 'git@bitbucket.org:beemol/isharingsdk.git', :tag => s.version.to_s }
 
  s.ios.deployment_target = '10.0'
  s.source_files = 'iSharingSDK/**/*'
 
end
