//
//  SessionViewController.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import UIKit

/** The view controller responsible for rendering a Kis presentation session.
 */
public class KisSessionViewController: UIViewController {

    /// The Kis session object to be rendered. This property should be set once, typically in the prepareForSegue method of the pushing controller.
    public var session: KisSession!
    
    deinit {
        if !session.exited {
            session.exit()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
