//
//  Session.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import Foundation

/** An object that represents an iSharing presentation session.
 */
@objcMembers
public final class KisSession: NSObject {
    
    /// A Boolean value that indicates whether the receiver was exited by the user. Its value should be set only once.
    dynamic var exited = false
    
    /// The designated initializer.
    ///
    /// - Parameter sessionManifest: a `SessionManifest` data structure
    init(sessionManifest: KisSessionManifest) {
        //
    }
    
    func exit() {
        self.exited = true
    }
}
