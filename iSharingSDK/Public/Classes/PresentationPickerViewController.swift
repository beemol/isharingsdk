//
//  PresentationPickerViewController.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 17/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import UIKit

public class PresentationPickerViewController: UIViewController {

    let kisSessionRunner = KisSessionRunner()

    public var presentationProvider: PresentationFolderProviding!
    
    var selectedPresentations: [Presentation]?
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sessionViewController = segue.destination as? KisSessionViewController {
            sessionViewController.session = kisSessionRunner.currentKisSession
        }
    }

    @IBAction func pushSession(_ sender: Any) {
        selectedPresentations = [presentationProvider.rootFolder.contents().filter({$0 is Presentation}).first as! Presentation]
        let sessionManifest = KisSessionManifest(presentations: selectedPresentations!)
        activityIndicatorView.startAnimating()
        kisSessionRunner.initiateSession(withManifest: sessionManifest, success: { [weak self] in
            self?.activityIndicatorView.stopAnimating()
            self?.performSegue(withIdentifier: "Show Session", sender: self)
        }) { (error) in
            //
        }
    }
}
