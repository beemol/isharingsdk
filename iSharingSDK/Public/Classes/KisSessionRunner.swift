//
//  SessionRunner.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import Foundation

/// The `SessionRunner` class starts, pauses, and exits Kis sessions.
public class KisSessionRunner {
    
    weak var timer: Timer? // this is to simulate session establishment time

    public private(set) var currentKisSession: KisSession? {
        didSet {
            sessionExitObservation = currentKisSession?.observe(\.exited, changeHandler: { [weak self] (session, change) in
                if session.exited {
                    self?.exitSession(withSuccess: {
                        //
                    }, failure: { (error) in
                        //
                    })
                }
            })
        }
    }
    
    private var sessionExitObservation: NSKeyValueObservation?
    
    public init() {
    }

    public func initiateSession(withManifest manifest: KisSessionManifest,
                         success: @escaping () -> Void,
                         failure: @escaping (Error?) -> Void) -> Void {
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (timer) in
            self.currentKisSession = KisSession(sessionManifest: manifest)
            success()
        })
    }
    
    private func exitSession(withSuccess success: @escaping () -> Void, failure: @escaping (Error?) -> Void) -> Void {
        currentKisSession = nil
        success()
    }
}
