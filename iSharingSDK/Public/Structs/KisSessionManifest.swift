//
//  SessionManifest.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import Foundation

public struct KisSessionManifest {
    public let presentations: [Presentation]
    
    public init(presentations: [Presentation]) {
        self.presentations = presentations
    }
}
