//
//  PresentationProvider.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 12/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import Foundation

/** External objects that provide access to presentations shall conform to this protocol.
 
 Presentation providers are assumed to provide a tree structure with a root folder.
 */
public protocol PresentationFolderProviding: class {
    
    /// The root folder of the tree structure.
    var rootFolder: PresentationFolder { get }
}

/** The base protocol for the contents of a folder.
 
 */
public protocol PresentationFolderContent {
    /// The name of the content.
    var name: String { get }
}

/** The protocol for a folder in the tree structure.
 
 */
public protocol PresentationFolder: PresentationFolderContent {
    /// Asks the receiver for its contents.
    ///
    /// - Returns: The contents of a folder, ie. presentations and subfolders.
    func contents() -> [PresentationFolderContent]
}
