//
//  Content.swift
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import Foundation

/** Objects or structs that represents a presentation to be rendered in a Kis session must conform to this protocol.
 */
public protocol Presentation: PresentationFolderContent {

    /// A presentation consists of sequences, which span one or more pages of content.
    var sequences: [Sequence] { get }

    /// Asks the receiver for his thumbnail.
    ///
    /// - Returns: The presentation thumbnail.
    func thumbnail() -> UIImage?
}

/** An object that represents a sequence in a presentation, that is, one or more pages of content to be rendered in a Kis session.
 */
public protocol Sequence {

    /// Asks the receiver for his thumbnail.
    ///
    /// - Returns: The presentation thumbnail.
    func thumbnail() -> UIImage?
}
