//
//  iSharingSDK.h
//  iSharingSDK
//
//  Created by Jean-Yves Vocisano on 09/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for iSharingSDK.
FOUNDATION_EXPORT double iSharingSDKVersionNumber;

//! Project version string for iSharingSDK.
FOUNDATION_EXPORT const unsigned char iSharingSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iSharingSDK/PublicHeader.h>


