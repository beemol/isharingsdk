#  iSharingSDK

__Kadrige iSharing__ is a general purpose, offline and online presentation system developed by Kadrige. __iSharingSDK__ is an iOS Swift module that exposes an API for use of the iSharing presentation feature by external systems.

## Installation
iSharingSDK is distributed as a Swift package.

TODO: use Carthage if possible because it's less intrusive, ie. it doesn't need a special Xcode project.

## Usage
The SDK exposes a number of public classes and protocols, as well as graphical resources in the form of storyboards.

Note that we are adopting another terminology here, modelled after that of the target host app:

- Kis contents are renamed presentations;
- content slides are called sequences.

To run a Kis Session, do the following:

1. Use a Kis Session Runner object to initiate a Kis session (assuming the presentations(aka contents) are set by the host application)
2. Once a Kis Session is inititiated, push the session view controller provided by the SDK in the KisSession.storyboard file (see code in the sample SDKUsingApp)

To use the provided presentation picker, do the following:

1. Create classes and objects conforming to the Presentation, PresentationFolder, and PresentationProviding :
2. Push the presentation picker view controller provided by the SDK in the PresentationPicker.storyboard file (see code in the sample SDKUsingApp)
