//
//  ViewController.swift
//  SDKUsingApp
//
//  Created by Jean-Yves Vocisano on 11/04/2018.
//  Copyright © 2018 Kadrige. All rights reserved.
//

import UIKit

import iSharingSDK

class ViewController: UIViewController {
    
    @IBOutlet weak var sessionBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    let kisSessionRunner = KisSessionRunner()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sessionViewController = segue.destination as? KisSessionViewController {
            sessionViewController.session = kisSessionRunner.currentKisSession
        } else if let presentationPicker = segue.destination as? PresentationPickerViewController {
            presentationPicker.presentationProvider = MockPresentationProvider()
        }
    }

    @IBAction func selectContents(_ sender: Any) {
        self.performSegue(withIdentifier: "Show Content Picker", sender: self)
    }
    
    @IBAction func pushSession(_ sender: Any) {
        let sessionManifest = KisSessionManifest(presentations: [Presentation]())
        activityIndicatorView.startAnimating()
        kisSessionRunner.initiateSession(withManifest: sessionManifest, success: { [weak self] in
            self?.activityIndicatorView.stopAnimating()
            self?.performSegue(withIdentifier: "Show Session", sender: self)
        }) { (error) in
            //
        }
    }
    
}
// MARK: Mock Objects

class MockPresentationProvider: PresentationFolderProviding {
    var rootFolder: PresentationFolder {
        return MockPresentationFolder()
    }
}

class MockPresentationFolder: PresentationFolder {
    var name = "root"
    
    func contents() -> [PresentationFolderContent] {
        let prez1 = MockPresentation()
        prez1.name = "prez1"
        let prez2 = MockPresentation()
        prez2.name = "prez2"
        return [prez1, prez2]
    }
}

class MockPresentation: Presentation {
    var sequences: [iSharingSDK.Sequence] = [MockSequence]()
    
    func thumbnail() -> UIImage? {
        return nil
    }
    
    var name: String = ""
}

class MockSequence: iSharingSDK.Sequence {
    func thumbnail() -> UIImage? {
        return nil
    }
    
    var name: String = ""
}
